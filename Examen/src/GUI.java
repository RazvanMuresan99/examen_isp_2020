import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class GUI {
    private static JTextField fisier, text;
    private static JButton buton;

    public static void main(String[] args) {
        JFrame frame=new JFrame("app");
        frame.setSize(500,500);
        frame.getContentPane().setLayout(new FlowLayout());

        fisier=new JTextField("Fisier");
        fisier.setSize(200,50);
        text=new JTextField("Text");
        text.setSize(200,50);

        buton = new JButton("Scrie text");
        buton.addActionListener(e -> {
            File file = new File(fisier.getText());

            try {
                try (FileReader reader = new FileReader(file)) {
                    int cnt = 0;
                    while(reader.read() != 0)
                        cnt += 1;

                    text.setText(String.valueOf(cnt));
                }
            } catch (IOException | HeadlessException z) {
                JOptionPane.showMessageDialog(null, e);
            }

        });


        frame.add(fisier);
        frame.add(text);
        frame.add(buton);
        frame.setVisible(true);
    }
}
