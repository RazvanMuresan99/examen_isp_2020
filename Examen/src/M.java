class A{
    private M m;
    public A(M m){
        this.m = m;
    }

    public void metA(){}
}

class B{

    public void metB(){}
}

class L{
    int a;
    private M m;
    public L(){

    }

    public void setM(M m){
        this.m = m;
    }

    public void i(X x){

    }

}

class X{

}

class K{

}

public class M extends K{
    private B b;
    public M(){
        b = new B();
    }
}
